'use strict';

var windowTypes = {
    about: 'ABOUT',
    captcha: 'CAPTCHA',
    essentia: 'ESSENTIA',
    invite: 'INVITE',
    mail: 'MAIL',
    notes: 'NOTES',
    options: 'OPTIONS',
    promotions: 'PROMOTIONS',
    serverClock: 'SERVER_CLOCK',
    stats: 'STATS'
};

module.exports = windowTypes;
