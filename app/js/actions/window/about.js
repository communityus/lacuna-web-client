'use strict';

var Reflux = require('reflux');

var AboutActions = Reflux.createActions([
    'load'
]);

module.exports = AboutActions;
